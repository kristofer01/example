package com.udea.example.example.service;

import com.udea.example.example.dto.PersonDto;
import com.udea.example.example.entity.Person;
import com.udea.example.example.repository.PersonRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    private PersonRepository personRepository;

    private ModelMapper modelMapper;


    public PersonService(PersonRepository personRepository, ModelMapper modelMapper) {
        this.personRepository = personRepository;
        this.modelMapper = modelMapper;
    }

    public PersonDto create(PersonDto personToCreateDto) {
        LOGGER.debug("Begin create: personToCreateDto={}", personToCreateDto);

        Person personToCreate = modelMapper.map(personToCreateDto, Person.class);
        Person result = personRepository.save(personToCreate);
        PersonDto resultDTO = modelMapper.map(result, PersonDto.class);

        LOGGER.debug("End create: resultDTO={}", resultDTO);
        return resultDTO;
    }

    public List<PersonDto> findAll() {
        List<Person> listPeople = (List<Person>) personRepository.findAll();
        return listPeople.stream()
                .map(person -> modelMapper.map(person, PersonDto.class))
                .collect(Collectors.toList());
    }

    public PersonDto findById(Long id) {
        Optional<Person> findPersonOptional = personRepository.findById(id);
        Person findPerson = findPersonOptional.orElseThrow(EntityNotFoundException::new);
        return modelMapper.map(findPerson, PersonDto.class);
    }

    public void delete(Long id) {
        PersonDto personInDb = findById(id);
        Person personToDelete = modelMapper.map(personInDb, Person.class);
        personRepository.delete(personToDelete);
    }

    public PersonDto update(PersonDto personDto) {
        findById(personDto.getId());
        return create(personDto);
    }


}
